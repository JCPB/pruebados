from django.conf import settings
from django.conf.urls.static import static

from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('login', views.login,name="login"),
    path('login/iniciar',views.login_iniciar,name="iniciar"),
    path('login/resetpass',views.resetpass,name="resetpass"),
    path('reset',views.reset,name="reset"),
    path('cerrarsession',views.cerrar_session,name="cerrar_session"),
     path('index/crear',views.crear,name="crear"),
    path('index/postDog',views.postDog,name="postDog"),
    path('config',views.config,name="config"),
    path('dogs',views.dogs,name='dogs'),
    path('auth_validate', views.auth_validate, name='auth_validate'),
    path('editDog',views.editDog,name='editDog'),
    path('eraseDog',views.eraseDog,name='eraseDog'),
    path('editar',views.editar,name='editar'),
    path('adoptar',views.adoptar,name='adoptar'),
    path('funcion1',views.under_construction, name='funcion1'),
    path('funcion2',views.under_construction, name='funcion2'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) 
