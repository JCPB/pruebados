function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
};

var csrftoken = getCookie('csrftoken');

console.log(csrftoken);

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});

$('#userpanel').click(function(){
    var p = document.getElementById('userpanel');
    console.log(p);
    if (p.classList.contains('panelopen')){
        $('#userpanel').removeClass('panelopen')
    }
    else{
        $('#userpanel').addClass('panelopen')
    }
});
$.validator.addMethod("minAge", function(value, element, min) {
    var today = new Date();
    var birthDate = new Date(value);
    var age = today.getFullYear() - birthDate.getFullYear();
 
    if (age > min+1) {
        return true;
    }
 
    var m = today.getMonth() - birthDate.getMonth();
 
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
 
    return age >= min;
},"mensaje");

function formatrut(rut){
    var valor = rut.value.replace('.','');
    valor = valor.replace('-','');
    cuerpo = valor.slice(0,-1);
    dv = valor.slice(-1).toUpperCase();
    rut.value = cuerpo + '-'+ dv
     
}

function checkRut(rut) {
    rut = String(rut);
    var valor = rut.replace(".", "").replace(".", "");
    valor = valor.replace("-", "");
    cuerpo = valor.slice(0, -1);
    dv = valor.slice(-1).toUpperCase();
    rut = cuerpo + "-" + dv;
    if (cuerpo.length < 7) {
      return false;
    }
    suma = 0;
    multiplo = 2;
    for (i = 1; i <= cuerpo.length; i++) {
      index = multiplo * valor.charAt(cuerpo.length - i);
      suma = suma + index;
      if (multiplo < 7) {
        multiplo = multiplo + 1;
      } else {
        multiplo = 2;
      }
    }
    dvEsperado = 11 - suma % 11;
    dv = dv == "K" ? 10 : dv;
    dv = dv == 0 ? 11 : dv;
    if (dvEsperado != dv) {
      return false;
    }
    return true;
}

$.validator.addMethod("validRut", function(value, element) {
    return checkRut(value);
}, );

$("#formaregistro").validate({
    rules: {
        username:{
            required:true,
 
        },
        correo:{
            required: true,
            email:true,
        },
        recorreo:{
            required: true,
            equalTo: "#correo",
        },
        nombre: {
            required:true,
            minlength:4,
            maxlength:20
        },
        apellidop: {
            required:true,
            minlength:4,
            maxlength:20
        },
        apellidom: {
            required:true,
            minlength:4,
            maxlength:20
        },
        fnac:{
            required:true,
            minAge:18,
        },
        tel:{
            required:true,
        },
       
       region:{
           required:true,
       },
       comuna:{
           required:true,
       },
       rut:{
           required:true,
           validRut: true
       },
       vivienda:{
           required:true
       },
       foto:{
           required:true
       },
       contrasenia:{
           required:true,
           minlength:8,
            maxlength:20
       },
       recontrasenia:{
            required:true,
            equalTo:'#contrasenia'
       },
       
            
    },
    messages: {
        username:{
            required:'Ingrese nombre de usuario',
            
        },
        correo:{
            required:"Se requiere un correo",
            email:"Correo invalido"
        },
        recorreo:{
            required: "Repita su email",
            equalTo: "No coincide",
            email:"Correo invalido"
        },
        nombre:{
            required:"Se requiere un nombre",
            minlength:"Se requiere al menos {0} caracteres ",
            maxlength:"Demaciados caracteres"
        },
        apellidop:{
            required:"Se requiere su apellido",
            minlength:"Se requiere al menos {0} caracteres ",
            maxlength:"Demaciados caracteres"
        },
        apellidom:{
            required:"Se requiere su apellido",
            minlength:"Se requiere al menos {0} caracteres ",
            maxlength:"Demaciados caracteres"
        },
        region:{
            required:"Se requiere una region"
        },
        comuna:{
            required:"Se requiere una comuna"
        },
        rut:{
            required:"Se requiere su rut",
            validRut:"Rut no valido"
        },
        vivienda:{
            required:"Por favor indique su tipo de vivienda"
        },
        fnac:{
            required:"Por favor indique su fecha de nacimiento",
            minAge:"Debes ser mayor de edad"
        },
        tel:{
            required:"Por favor ingrese su telefono"
        },
        foto:{
            required:"Agregue su foto"
        },
        contrasenia:{
            required:"Introduzca contraseña",
            minlength:"Minimo 8 caracteres",
            maxlength:"Maximo 20 caracteres"
        },
        recontrasenia:{
             required:'Repita contraseña',
             equalTo:'No coincide'
        },
    }      
});

$.validator.addMethod('user_exist',function(value,element){
    consultarusername(value);
},);



function consultarusername(value){
    var s = false;
    $.ajax({
                type: "POST",
                url: "/auth_validate",
                data: {'username': value, 'csrfmiddlewaretoken': csrftoken},
                dataType: "text", async: false,
                success: function(response) {
                    var response = $.parseJSON( response );
                    s = (response.success);}
                
    });
    return s;
}

/*
$("#nombre").keypress(function(event){
    var inputValue = event.which;
    if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)) { 
        event.preventDefault(); 
    }
});
*/

$("#nombre").keypress(function(event){
    var inputValue = event.which;
    if(!(inputValue >= 65 && inputValue <= 90)  && !(inputValue >= 97 && inputValue <= 122) && (inputValue != 32 && inputValue != 0) && inputValue != 209 && inputValue != 241) { 
        event.preventDefault(); 
    }
});

$("#apellidop").keypress(function(event){
    var inputValue = event.which;
    if(!(inputValue >= 65 && inputValue <= 90)  && !(inputValue >= 97 && inputValue <= 122) && inputValue != 0 && inputValue != 209 && inputValue != 241) { 
        event.preventDefault(); 
    }
});

$("#apellidom").keypress(function(event){
    var inputValue = event.which;
    if(!(inputValue >= 65 && inputValue <= 90)  && !(inputValue >= 97 && inputValue <= 122) && inputValue != 0 && inputValue != 209 && inputValue != 241) { 
        event.preventDefault(); 
    }
});

$("#rut").keypress(function(event){
    var inputValue = event.which;
    if(!(inputValue >= 48 && inputValue <= 57)  && inputValue != 0 && inputValue != 75 && inputValue != 107) { 
        event.preventDefault(); 
    }
});
$("#tel").keypress(function(event){
    var inputValue = event.which;
    if(!(inputValue >= 48 && inputValue <= 57)  && inputValue != 0) { 
        event.preventDefault(); 
    }
});

$(function(){

    regiones.regiones.forEach(region => {
        $("#region").append('<option value="'+region.region+'">'+region.region+'</option>')
    });
    console.log('region ready');
})

$("#region").change(function(){
    $("#comuna").html("")
    var region = $(this).val()
    var comunas = regiones.regiones.find(item => item.region == region);
    $("#comuna").append('<option value="" >Seleccione</option>')
    comunas.comunas.sort().forEach(comuna => {
        $("#comuna").append('<option value="'+comuna+'">'+comuna+'</option>')
    });
})

var regiones = {
    "regiones": [{
        "region": "Arica y Parinacota",
        "comunas": ["Arica", "Camarones", "Putre", "General Lagos"]
    },
    {
        "region": "Tarapacá",
        "comunas": ["Iquique", "Alto Hospicio", "Pozo Almonte", "Camiña", "Colchane", "Huara", "Pica"]
    },
    {
        "region": "Antofagasta",
        "comunas": ["Antofagasta", "Mejillones", "Sierra Gorda", "Taltal", "Calama", "Ollagüe", "San Pedro de Atacama", "Tocopilla", "María Elena"]
    },
    {
        "region": "Atacama",
        "comunas": ["Copiapó", "Caldera", "Tierra Amarilla", "Chañaral", "Diego de Almagro", "Vallenar", "Alto del Carmen", "Freirina", "Huasco"]
    },
    {
        "region": "Coquimbo",
        "comunas": ["La Serena", "Coquimbo", "Andacollo", "La Higuera", "Paiguano", "Vicuña", "Illapel", "Canela", "Los Vilos", "Salamanca", "Ovalle", "Combarbalá", "Monte Patria", "Punitaqui", "Río Hurtado"]
    },
    {
        "region": "Valparaíso",
        "comunas": ["Valparaíso", "Casablanca", "Concón", "Juan Fernández", "Puchuncaví", "Quintero", "Viña del Mar", "Isla de Pascua", "Los Andes", "Calle Larga", "Rinconada", "San Esteban", "La Ligua", "Cabildo", "Papudo", "Petorca", "Zapallar", "Quillota", "Calera", "Hijuelas", "La Cruz", "Nogales", "San Antonio", "Algarrobo", "Cartagena", "El Quisco", "El Tabo", "Santo Domingo", "San Felipe", "Catemu", "Llaillay", "Panquehue", "Putaendo", "Santa María", "Quilpué", "Limache", "Olmué", "Villa Alemana"]
    },
    {
        "region": "Región del Libertador Gral. Bernardo O’Higgins",
        "comunas": ["Rancagua", "Codegua", "Coinco", "Coltauco", "Doñihue", "Graneros", "Las Cabras", "Machalí", "Malloa", "Mostazal", "Olivar", "Peumo", "Pichidegua", "Quinta de Tilcoco", "Rengo", "Requínoa", "San Vicente", "Pichilemu", "La Estrella", "Litueche", "Marchihue", "Navidad", "Paredones", "San Fernando", "Chépica", "Chimbarongo", "Lolol", "Nancagua", "Palmilla", "Peralillo", "Placilla", "Pumanque", "Santa Cruz"]
    },
    {
        "region": "Región del Maule",
        "comunas": ["Talca", "Constitución", "Curepto", "Empedrado", "Maule", "Pelarco", "Pencahue", "Río Claro", "San Clemente", "San Rafael", "Cauquenes", "Chanco", "Pelluhue", "Curicó", "Hualañé", "Licantén", "Molina", "Rauco", "Romeral", "Sagrada Familia", "Teno", "Vichuquén", "Linares", "Colbún", "Longaví", "Parral", "Retiro", "San Javier", "Villa Alegre", "Yerbas Buenas"]
    },
    {
        "region": "Región de Ñuble",
        "comunas": ["Cobquecura", "Coelemu", "Ninhue", "Portezuelo", "Quirihue", "Ránquil", "Treguaco", "Bulnes", "Chillán Viejo", "Chillán", "El Carmen", "Pemuco", "Pinto", "Quillón", "San Ignacio", "Yungay", "Coihueco", "Ñiquén", "San Carlos", "San Fabián", "San Nicolás"]
    },
    {
        "region": "Región del Biobío",
        "comunas": ["Concepción", "Coronel", "Chiguayante", "Florida", "Hualqui", "Lota", "Penco", "San Pedro de la Paz", "Santa Juana", "Talcahuano", "Tomé", "Hualpén", "Lebu", "Arauco", "Cañete", "Contulmo", "Curanilahue", "Los Álamos", "Tirúa", "Los Ángeles", "Antuco", "Cabrero", "Laja", "Mulchén", "Nacimiento", "Negrete", "Quilaco", "Quilleco", "San Rosendo", "Santa Bárbara", "Tucapel", "Yumbel", "Alto Biobío"]
    },
    {
        "region": "Región de la Araucanía",
        "comunas": ["Temuco", "Carahue", "Cunco", "Curarrehue", "Freire", "Galvarino", "Gorbea", "Lautaro", "Loncoche", "Melipeuco", "Nueva Imperial", "Padre las Casas", "Perquenco", "Pitrufquén", "Pucón", "Saavedra", "Teodoro Schmidt", "Toltén", "Vilcún", "Villarrica", "Cholchol", "Angol", "Collipulli", "Curacautín", "Ercilla", "Lonquimay", "Los Sauces", "Lumaco", "Purén", "Renaico", "Traiguén", "Victoria"]
    },
    {
        "region": "Región de Los Ríos",
        "comunas": ["Valdivia", "Corral", "Lanco", "Los Lagos", "Máfil", "Mariquina", "Paillaco", "Panguipulli", "La Unión", "Futrono", "Lago Ranco", "Río Bueno"]
    },
    {
        "region": "Región de Los Lagos",
        "comunas": ["Puerto Montt", "Calbuco", "Cochamó", "Fresia", "Frutillar", "Los Muermos", "Llanquihue", "Maullín", "Puerto Varas", "Castro", "Ancud", "Chonchi", "Curaco de Vélez", "Dalcahue", "Puqueldón", "Queilén", "Quellón", "Quemchi", "Quinchao", "Osorno", "Puerto Octay", "Purranque", "Puyehue", "Río Negro", "San Juan de la Costa", "San Pablo", "Chaitén", "Futaleufú", "Hualaihué", "Palena"]
    },
    {
        "region": "Región Aisén del Gral. Carlos Ibáñez del Campo",
        "comunas": ["Coihaique", "Lago Verde", "Aisén", "Cisnes", "Guaitecas", "Cochrane", "O’Higgins", "Tortel", "Chile Chico", "Río Ibáñez"]
    },
    {
        "region": "Región de Magallanes y de la Antártica Chilena",
        "comunas": ["Punta Arenas", "Laguna Blanca", "Río Verde", "San Gregorio", "Cabo de Hornos (Ex Navarino)", "Antártica", "Porvenir", "Primavera", "Timaukel", "Natales", "Torres del Paine"]
    },
    {
        "region": "Región Metropolitana de Santiago",
        "comunas": ["Cerrillos", "Cerro Navia", "Conchalí", "El Bosque", "Estación Central", "Huechuraba", "Independencia", "La Cisterna", "La Florida", "La Granja", "La Pintana", "La Reina", "Las Condes", "Lo Barnechea", "Lo Espejo", "Lo Prado", "Macul", "Maipú", "Ñuñoa", "Pedro Aguirre Cerda", "Peñalolén", "Providencia", "Pudahuel", "Quilicura", "Quinta Normal", "Recoleta", "Renca", "San Joaquín", "San Miguel", "San Ramón", "Vitacura", "Puente Alto", "Pirque", "San José de Maipo", "Colina", "Lampa", "Tiltil", "San Bernardo", "Buin", "Calera de Tango", "Paine", "Melipilla", "Alhué", "Curacaví", "María Pinto", "San Pedro", "Talagante", "El Monte", "Isla de Maipo", "Padre Hurtado", "Peñaflor"]
        }]
    }


		    
