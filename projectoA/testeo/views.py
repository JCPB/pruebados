from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect

from django.http import JsonResponse

from .models import Persona,Dog

from django.core.mail import EmailMessage

from django.utils.crypto import get_random_string

from django.contrib.auth.models import User
from django.contrib.auth import authenticate,logout, login as auth_login
from django.contrib.auth.decorators import login_required
# Create your views here.

def index(request):
	usuario = request.session.get('usuario',None)
	if usuario is not None:
		user = request.user
		userimage = user.persona.foto
		can_adopt = user.persona.can_adopt
		is_admin = user.persona.is_admin
		return render(request, 'index.html',{'can_adopt':can_adopt,'is_admin':is_admin ,'personas':Persona.objects.all(),'userimage':userimage,'perros':Dog.objects.filter(estado='DI'),'usuario':usuario})
	else:
		return render(request,'index.html',{'perros':Dog.objects.filter(estado='DI')})

@login_required(login_url='login')
def config(request):
	usuario = request.user
	if usuario is not None:
		return render(request,'settings.html',{'usuario':usuario})
	else:
		return redirect("index")

def login(request):
    return render(request,'login.html',{})

@login_required(login_url='login')
def dogs(request):
	usuario = request.user
	if usuario.persona.is_admin:
		return render(request,'dogs.html',{'perros':Dog.objects.all()})
	else:
		return redirect("index")

def login_iniciar(request):
    usuario = request.POST.get('nombre_usuario','')
    contrasenia = request.POST.get('contrasenia','')
    user = authenticate(request,username=usuario, password=contrasenia)

    if user is not None:
        auth_login(request, user)
        request.session['usuario'] = user.first_name+" "+user.last_name
        return redirect("index")
    else:
        return redirect("login")

@login_required(login_url='login')
def cerrar_session(request):
    del request.session['usuario']
    logout(request)
    return redirect('index')

def crear(request):
	nombre = request.POST.get('nombre','')
	correo = request.POST.get('correo','')
	apellidop = request.POST.get('apellidop','')
	apellidom = request.POST.get('apellidom','')
	telefono = request.POST.get('tel','')
	rut = request.POST.get('rut','')
	region = request.POST.get('region','')
	comuna = request.POST.get('comuna','')
	vivienda = request.POST.get('vivienda','')
	fnac = request.POST.get('fnac','')
	contrasenia = request.POST.get('contrasenia','')
	username = request.POST.get('username','')
	foto = request.FILES.get('foto',False)
	can_adopt = True

	user = User(first_name=nombre,last_name=apellidop,username=username,email=correo)
	user.set_password(contrasenia)
	user.save()
	
	persona = Persona(can_adopt=can_adopt,user=user,fnac=fnac,vivienda=vivienda,comuna=comuna,region=region,rut=rut,telefono=telefono,apellidom=apellidom,foto = foto)
	persona.save()

	email = EmailMessage('My Perris Register', 'Felicitaciones '+ nombre +' '+ apellidop +' ya estas registrado, para mas informacion y conocer nuestros cachorros ingresa a nuestro Sitio', to=[correo])
	email.send()
	return redirect('index')

def resetpass(request):
	return render(request,'resetpass.html',{})

def reset(request):
	newpass = get_random_string(length=8, allowed_chars='ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890')
	user_name = request.POST.get('nombre_usuario','')
	user = User.objects.get(username=user_name)
	email = EmailMessage('My Perris Reset Password', 'su nueva contraseña es: '+ newpass, to=[user.email])
	email.send()
	user.set_password(newpass)
	user.save()
	return redirect('login')

@login_required(login_url='login')
def postDog(request):
	usuario = request.user

	if usuario.persona.is_admin :
		nombre = request.POST.get('nombre','')
		raza = request.POST.get('raza','')
		foto = request.FILES.get('foto',False)
		descripcion = request.POST.get('descripcion','')
		estado = request.POST.get('estado','RE')
		perro = Dog(nombre=nombre,foto=foto,raza=raza,descripcion=descripcion,estado=estado,autor=usuario.persona)
		perro.save()
	return redirect('index')

@login_required(login_url='login')
def adoptar(request):
	usuario = request.user
	if usuario.persona.can_adopt :
		dogId = request.POST.get('pk','')
		perro = Dog.objects.get(pk=dogId)
		perro.adopter = usuario.persona
		perro.estado = 'AD'
		perro.save()
		return redirect('index')


def auth_validate(request):
	success = False
	if request.method == 'POST':
		username = request.POST.get('username', None)
		if User.objects.filter(username=username).exists():
			success = True
		else:
			success = False
		
	return JsonResponse({'success': success})

@login_required(login_url='login')
def editDog(request):
	usuario = request.user
	if usuario.persona.is_admin :
		dogId = request.POST.get('pk','')
		nombre = request.POST.get('nombre','')
		descripcion = request.POST.get('descripcion','')
		estado = request.POST.get('estado','')
		perro = Dog.objects.get(pk=dogId)
		perro.nombre = nombre
		perro.descripcion = descripcion
		perro.estado = estado
		perro.save()
		return redirect('dogs')

@login_required(login_url='login')
def eraseDog(request):
	usuario = request.user
	if usuario.persona.is_admin :
		dogId = request.POST.get('pk','')
		perro = Dog.objects.get(pk=dogId)
		perro.delete()
		return redirect('dogs')

@login_required(login_url='login')
def editar(request):
	u = request.user
	usuario = User.objects.get(username=u.username)
	nombre = request.POST.get('nombre','')
	apellidop = request.POST.get('apellidop','')
	apellidom = request.POST.get('apellidom','')
	telefono = request.POST.get('tel','')
	region = request.POST.get('region','')
	comuna = request.POST.get('comuna','')
	vivienda = request.POST.get('vivienda','')
	contrasenia = request.POST.get('contrasenia','')
	foto = request.FILES.get('foto',False)
	if (nombre != ''):
		usuario.first_name=nombre
	if (apellidop != ''):
		usuario.last_name=apellidop
	if (apellidom != ''):
		usuario.persona.apellidom=apellidom
	if (telefono != ''):
		usuario.persona.telefono=telefono
	if (region != ''):
		usuario.persona.region=region
	if (comuna != ''):
		usuario.persona.comuna=comuna
	if (vivienda != ''):
		usuario.persona.vivienda=vivienda
	if (foto != False):
		usuario.persona.foto=foto
	if (contrasenia != ''):
		del request.session['usuario']
		logout(request)
		print(contrasenia)
		u= User.objects.get(username=usuario.username)
		u.set_password(contrasenia)
		u.save()
		u.persona.save()
		email = EmailMessage('My Perris cambio de contraseña', 'su nueva contraseña es: '+ contrasenia, to=[u.email])
		email.send()
		return redirect('login')
	usuario.save()
	usuario.persona.save()
	return redirect('index')

def under_construction(request):
	return render(request,'under_construction.html')