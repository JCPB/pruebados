from django.apps import AppConfig


class TesteoConfig(AppConfig):
    name = 'testeo'
